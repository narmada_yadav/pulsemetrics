//
//  SecondController.swift
//  PulseSDKTesterApp
//
//  Created by mac on 14/02/19.
//  Copyright © 2019 Mobile Engineer Relibit Labs. All rights reserved.
//

import UIKit

class SecondController: UIViewController {

    @IBOutlet weak var tempView : UIView!
    @IBOutlet weak var scrollView : UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: tempView.frame.height)
    }
}





