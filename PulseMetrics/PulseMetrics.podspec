Pod::Spec.new do |s|

s.name         = "PulseMetrics"
s.version      = "0.0.4"
s.summary      = "PulseMetrics sdk is use track analytics."
s.description  = <<-DESC
PulseMetrics SDK is use for track iOS application analytics & events.
DESC

s.homepage     = "https://github.com/ddevjani"
s.license      = { :type => "MIT", :file => "license" }
s.author             = { "NP Yadav" => "narmada.yadav@linkites.com" }
s.ios.deployment_target = '11.0'
s.ios.vendored_frameworks = 'PulseMetrics.framework'
s.source            = { :http => 'http://159.89.164.34:4100/files/PulseMetrics.zip' }
s.exclude_files = "Classes/Exclude"
s.ios.dependency 'Socket.IO-Client-Swift'
end
