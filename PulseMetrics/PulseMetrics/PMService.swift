//
//  PMService.swift
//  PulseMetrics
//
//  Created by mac on 2/13/19.
//  Copyright © 2019 Mobile Engineer Relibit Labs. All rights reserved.
//

import Foundation
import UIKit

public enum PMHTTPRequest {
    static let Get    = "GET"
    static let Post   = "POST"
    static let Put    = "PUT"
    static let Delete = "DELETE"
}

private struct PMConfig {
    static let baseUrl = "http://159.89.164.34:4100/api/v1/"     // Live
    //    static let baseUrl = "http://192.168.1.79:4100/api/v1/"     // Staging
}

open class PMService: NSObject {
    
    open class func getApiRequest(url: String, completion: @escaping (Bool?, PMAddress?) -> (Void)) {
        guard let url = URL(string: url) else {
            print("GET Error: cannot create URL")
            completion(false, nil)
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                print("GET Error: \(error?.localizedDescription ?? "")")
                completion(false, nil)
                return
            }
            guard let data = data else {
                print("GET Error: did not receive data")
                completion(false, nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let address = try decoder.decode(PMAddress.self, from: data)
                completion(true, address)
            } catch  {
                print("GET Error trying to parse data to JSON")
                completion(false, nil)
                return
            }
            }.resume()
    }
    
    open class func postApiRequest(url: String, parameters: [String: Any], completion: @escaping (Bool?) -> (Void)) {
        guard let url = URL(string: "\(PMConfig.baseUrl)\(url)") else {
            print("POST Error: cannot create URL")
            completion(false)
            return
        }
        PulseMetrics.log("url: \(url) *** param: \(parameters)")
        var request = URLRequest(url: url)
        request.httpMethod = PMHTTPRequest.Post
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let jsonData: Data
        do {
            jsonData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            request.httpBody = jsonData
        } catch {
            print("POST Error: cannot create JSON from data")
            completion(false)
            return
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("POST Error: \(error?.localizedDescription ?? "")")
                completion(false)
                return
            }
            guard let data = data else {
                print("POST Error: did not receive data")
                completion(false)
                return
            }
            do {
                guard let responseData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    print("POST Could not get JSON from responseData as dictionary")
                    completion(false)
                    return
                }
                completion(true)
                print("POST ResponseData: \(responseData)")
            } catch  {
                print("POST Error trying to parse data to JSON")
                completion(false)
                return
            }
            }.resume()
    }
    
    open class func multiPartApiRequest(url: String, img: UIImage?, parameters: [String : Any], completion: @escaping (Bool?) -> (Void)) {
        guard let url = URL(string: "\(PMConfig.baseUrl)\(url)") else {
            print("POST Error: cannot create URL")
            completion(false)
            return
        }
        let paramName = "file"
        let boundary = UUID().uuidString
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = PMHTTPRequest.Post
        var data = Data()
        
        // For image data
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(parameters["className"] as! String)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(img?.pngData()! ?? Data())
        
        // For other params
        for (key, value) in parameters{
            data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
            data.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
            data.append("\(value)".data(using: .utf8)!)
        }
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        // Send a POST request to the URL, with the data we created earlier
        URLSession.shared.uploadTask(with: urlRequest, from: data) { (data, response, error)  in
            guard error == nil else {
                print("UPLOAD IMAGE Error: \(error?.localizedDescription ?? "")")
                completion(false)
                return
            }
            guard let data = data else {
                print("UPLOAD IMAGE Error: did not receive data")
                completion(false)
                return
            }
            do {
                guard let responseData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    print("UPLOAD IMAGE Could not get JSON from responseData as dictionary")
                    completion(false)
                    return
                }
                print("UPLOAD IMAGE ResponseData: \(responseData)")
                completion(true)
            } catch  {
                print("UPLOAD IMAGE Error trying to parse data to JSON")
                completion(false)
            }
            }.resume()
    }
    
    open class func putApiRequest(url: String, parameters: [String: Any]) {
        guard let url = URL(string: url) else {
            print("PUT Error: cannot create URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = PMHTTPRequest.Put
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let jsonData: Data
        do {
            jsonData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            request.httpBody = jsonData
        } catch {
            print("PUT Error: cannot create JSON from data")
            return
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("PUT Error: \(error?.localizedDescription ?? "")")
                return
            }
            guard let data = data else {
                print("PUT Error: did not receive data")
                return
            }
            do {
                guard let responseData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    print("PUT Could not get JSON from responseData as dictionary")
                    return
                }
                print("PUT ResponseData: \(responseData)")
            } catch  {
                print("PUT Error trying to parse data to JSON")
                return
            }
            }.resume()
    }
    
    open class func deleteApiRequest(url: String, parameters: [String: Any]) {
        guard let url = URL(string: url) else {
            print("DELETE Error: cannot create URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = PMHTTPRequest.Delete
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let jsonData: Data
        do {
            jsonData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            request.httpBody = jsonData
        } catch {
            print("DELETE Error: cannot create JSON from data")
            return
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("DELETE Error: \(error?.localizedDescription ?? "")")
                return
            }
            guard let data = data else {
                print("DELETE Error: did not receive data")
                return
            }
            do {
                guard let responseData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    print("DELETE Could not get JSON from responseData as dictionary")
                    return
                }
                print("DELETE ResponseData: \(responseData)")
            } catch  {
                print("DELETE Error trying to parse data to JSON")
                return
            }
            }.resume()
    }
}

public struct PMAddress: Codable {
    let city: String?
    let country: String?
    let countryCode: String?
    let lat: Double?
    let lon: Double?
    let org: String?
    let region: String?
    let regionName: String?
    let timezone: String?
    let zip: String?
    let abhishek: String?
}

