//
//  HomeController.swift
//  PulseSDKTesterApp
//
//  Created by mac on 2/13/19.
//  Copyright © 2019 Mobile Engineer Relibit Labs. All rights reserved.
//

import UIKit

class HomeController: UIViewController {

    @IBOutlet weak var lblCount: UILabel!
    
    var count: Double = 0.0
    var isCount = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(pauseTracking), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resumeTracking), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(endTracking), name: UIApplication.willTerminateNotification, object: nil)
        self.ab()
    }
    
    @IBAction func actionFirst(_ sender: Any) {
        print("action First")
        
    }
    
    @IBAction func actionSecond(_ sender: Any) {
        print("action Second")
        
    }
    
    @objc func resumeTracking() {
        self.isCount = true
        self.ab()
    }
    
    @objc func pauseTracking() {
        self.isCount = false
        print("pauseTracking self.count == \(self.lblCount.text!)")
    }
    
    @objc func endTracking() {
        self.isCount = false
        print("endTracking self.count == \(self.lblCount.text!)")
    }
    
    @objc func ab() {
        if isCount{
            self.count += 0.2
            self.lblCount.text = "\(self.count.roundOf())"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.ab()
            }
        }
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        return UIColor(red: .random(), green: .random(), blue: .random(), alpha: 1)
    }
}
extension Double {
    func roundOf() -> Double {
        let divisor = pow(10.0, Double(2))  // Round upto 4 value
        return (self * divisor).rounded() / divisor
    }
}
