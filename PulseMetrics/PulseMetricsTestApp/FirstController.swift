//
//  FirstController.swift
//  PulseSDKTesterApp
//
//  Created by mac on 14/02/19.
//  Copyright © 2019 Mobile Engineer Relibit Labs. All rights reserved.
//

import UIKit
import PulseMetrics

class FirstController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTmp", for: indexPath) as! cellTmp
        cell.img.backgroundColor = .random
        cell.lblTitle.text = "\(indexPath.row + 1) - " + self.randomString(length: 15)
        cell.lblSubtitle.text = self.randomString(length: 15)
        return cell
    }
    
    @IBOutlet weak var tblView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.tblView.reloadData()
    }
    
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
}

class cellTmp: UITableViewCell {
    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubtitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}


extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
