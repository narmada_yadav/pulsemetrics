//
//  PulseMetrics.swift
//  PulseMetrics
//
//  Created by mac on 2/13/19.
//  Copyright © 2019 Mobile Engineer Relibit Labs. All rights reserved.
//

import Foundation
import UIKit

private class ScreenShot {
    var className = ""
    var imgScreenshot: UIImage?
    var isUpload: Bool?
}


open class PulseMetrics: NSObject {
    
    fileprivate static var shared = PulseMetrics()
    
    fileprivate static var isTrackScreen: Bool?
    fileprivate static var isTrackSession: Bool?
    fileprivate static var isFileUpload: Bool?
    
    fileprivate static var pmWindow: UIWindow?
    fileprivate static var appDuration: Double = 0
    
    fileprivate static var sessionStartTime = ""
    fileprivate static var timerScreenshot: Timer?
    fileprivate static var screenShot: [ScreenShot] = []
    fileprivate static var viewName: [String] = []
    
    fileprivate static var sessionflow = [[String:String]]()
    
    fileprivate static var lastScreenName = ""
    fileprivate static var lastScreenDuration: Double = 0
    
    // Device Info
    private var deviceId    = ""
    private var deviceType  = ""
    private var deviceOs    = ""
    private var deviceModel = ""
    
    private var getCurrentDateTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
        return formatter.string(from: Date())
    }
    
    private var pulseAppId: String = ""
    
    
    // MARK:- Initialize Method ----
    open class func log(_ msg : Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        #if DEBUG
        let className = (fileName as NSString).lastPathComponent
        print("<\(className)> [#\(lineNumber)] \(functionName) | \(msg)\n")
        #endif
    }
    
    open class func start(_ applicationId: String) {
        shared.pulseAppId = applicationId
        
        shared.checkOldSession() //
        
        PulseMetrics.timerScreenshot = Timer.scheduledTimer(timeInterval: 1.0, target: PulseMetrics.shared, selector: #selector(getScreenInfo), userInfo: nil, repeats: true)
        
        sessionStartTime = shared.getCurrentDateTime
        
        shared.setupGeture()
        shared.setupObeserver()
        shared.getDeviceInfo()
        shared.startTrackingSession()
    }
    
    private func getDeviceInfo() {
        if let uId = UserDefaults.standard.value(forKey: UserDefaults.keys.uniqueId) as? String {
            deviceId = uId
        }
        else {
            deviceId = String.randomString()
        }
        UserDefaults.standard.set(deviceId, forKey: UserDefaults.keys.uniqueId)
        UserDefaults.standard.synchronize()
        // Config
        deviceType = UIDevice.current.systemName
        deviceOs = UIDevice.current.systemVersion
        deviceModel = UIDevice.modelName
        
        toGetCurrentAddress()
    }
}

// MARK:- Add Observer ----
extension PulseMetrics {
    
    private func setupObeserver() {
        NotificationCenter.default.addObserver(PulseMetrics.shared, selector: #selector(pauseTracking), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(PulseMetrics.shared, selector: #selector(resumeTracking), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(PulseMetrics.shared, selector: #selector(endTracking), name: UIApplication.willTerminateNotification, object: nil)
    }
    
    @objc func resumeTracking() {
        self.startTrackingSession()
    }
    
    @objc func pauseTracking() {
        self.stopTrackingSession(isEnd: false)
    }
    
    @objc func endTracking() {
        self.stopTrackingSession(isEnd: true)
        sleep(5) // For waiting to server response
    }
}

// MARK:- Add Gesture ----
extension PulseMetrics: UIGestureRecognizerDelegate {
    
    public func setupGeture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: nil)
        tapGesture.delegate = PulseMetrics.shared
        PulseMetrics.pmWindow = UIApplication.shared.pmWindow
        PulseMetrics.pmWindow?.addGestureRecognizer(tapGesture)
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let touchPoint = touch.location(in: PulseMetrics.pmWindow)
        print("touch Point: \(touchPoint.x)")
        return false
    }
    
}

// MARK:- Track Session ----
extension PulseMetrics {
    
    private func startTrackingSession() {
        PulseMetrics.isTrackScreen = true
        PulseMetrics.isTrackSession = true
        self.trackSessionDuration()
    }
    
    private func stopTrackingSession(isEnd: Bool) {
        PulseMetrics.isTrackScreen = false
        PulseMetrics.isTrackSession = false
        
        if isEnd {
            self.saveAppSessionToServer()
        }
    }
}

// MARK:- Session Handle ----
extension PulseMetrics {
    
    private func saveAppSessionToServer() {
        let sessionData: [String: Any] = [
            "applicationId" : PulseMetrics.shared.pulseAppId,
            "sessionId"     : NSUUID().uuidString,
            "deviceId"      : PulseMetrics.shared.deviceId,
            "start_time"    : PulseMetrics.sessionStartTime,
            "end_time"      : PulseMetrics.shared.getCurrentDateTime,
            "duration"      : PulseMetrics.appDuration.PMroundOf(),
            "sessionflow"       : PulseMetrics.sessionflow
        ]
        
        UserDefaults.standard.set(sessionData, forKey: UserDefaults.keys.oldSession)
        UserDefaults.standard.synchronize()
        
        sendSeesionToServer(dict: sessionData)
        
    }
    
    private func trackSessionDuration() {
        if PulseMetrics.isTrackScreen ?? false {
            PulseMetrics.appDuration += 1
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.trackSessionDuration()
            }
        }
    }
    
    private func checkOldSession() {
        if let sessionData = UserDefaults.standard.value(forKey: UserDefaults.keys.oldSession) as? [String : Any] {
            sendSeesionToServer(dict: sessionData)
        }
    }
    
    private func sendSeesionToServer(dict: [String: Any]) {
        
        print("sendSeesionToServer :\(dict)")
        
        PMService.postApiRequest(url: "users/session/create", parameters: dict) { (success) -> (Void) in
            guard let success = success else { return }
            if success {
                UserDefaults.standard.set(nil, forKey: UserDefaults.keys.oldSession)
                UserDefaults.standard.synchronize()
            }
        }
    }
}

// MARK:- Screen Capture Handle ----
extension PulseMetrics {
    
    @objc private func getScreenInfo() {
        var screenName = ""
        if let title = UIApplication.shared.pulseMobileCurrentView?.navigationItem.title, title.pmTrim().count != 0 {
            screenName = title
        }
        else {
            screenName = UIApplication.shared.pulseMobileCurrentView?.className ?? ""
        }
        //START
        print("current ScreenName :\(screenName)")
        
        if PulseMetrics.lastScreenName.count == 0
        {
            PulseMetrics.lastScreenName = screenName
            PulseMetrics.lastScreenDuration += 1
        }else if PulseMetrics.lastScreenName == screenName
        {
            PulseMetrics.lastScreenDuration += 1
        }else
        {
            PulseMetrics.sessionflow.append(["className ": PulseMetrics.lastScreenName, "screenDuration": "\(PulseMetrics.lastScreenDuration)"])
            PulseMetrics.lastScreenName = screenName
            PulseMetrics.lastScreenDuration = 1
        }
        //END
        
        
        let isFound = PulseMetrics.viewName.contains(screenName)
        if isFound == false {
            if screenName != "UIAlertController" {
                let screenShot = ScreenShot()
                screenShot.className = screenName
                screenShot.imgScreenshot = UIApplication.shared.pmScreenShot
                screenShot.isUpload = false
                PulseMetrics.screenShot.append(screenShot)
                
                self.saveScreenInfoToServer()
            }
        }
        
        
        
    }
    
    private func saveScreenInfoToServer() {
        if PulseMetrics.isFileUpload ?? false { return }
        if PulseMetrics.screenShot.count == 0 { return }
        PulseMetrics.log("saveScreenShot")
        PulseMetrics.isFileUpload = true
        
        let screenshot = PulseMetrics.screenShot[0]
        
        let dict: [String: Any] = [
            "applicationId" : PulseMetrics.shared.pulseAppId,
            "className"     : screenshot.className,
            "width"         : UIScreen.main.bounds.width,
            "height"        : UIScreen.main.bounds.height
        ]
        
        PMService.multiPartApiRequest(url: "users/create/screenshot", img: screenshot.imgScreenshot, parameters: dict) { (success) -> (Void) in
            PulseMetrics.isFileUpload = false
            guard success == true else { return }
            PulseMetrics.screenShot.remove(at: 0)
            PulseMetrics.viewName.append(screenshot.className)
        }
    }
}

// MARK:- Network Calls ----
extension PulseMetrics {
    
    private func toGetCurrentAddress() {
        PMService.getApiRequest(url: "http://ip-api.com/json") { (success, address) -> (Void) in
            guard success == true else {
                let location = ["city": "", "state": "", "country": Locale.current.regionCode ?? ""]
                self.toCreateUser(location: location)
                return
            }
            let location = [
                "city"    : address?.city ?? "",
                "state"   : address?.regionName ?? "",
                "country" : address?.country ?? ""
            ]
            self.toCreateUser(location: location)
        }
    }
    
    private func toCreateUser(location: [String: String]) {
        let dict: [String: Any] = [
            "applicationId" : PulseMetrics.shared.pulseAppId,
            "deviceId"      : PulseMetrics.shared.deviceId,
            "deviceType"    : PulseMetrics.shared.deviceType,
            "deviceOs"      : PulseMetrics.shared.deviceOs,
            "deviceModel"   : PulseMetrics.shared.deviceModel,
            "location"      : location
        ]
        PMService.postApiRequest(url: "users/create", parameters: dict) { (success) -> (Void) in
            print("toCreateUser: \(success ?? false)")
        }
    }
}

